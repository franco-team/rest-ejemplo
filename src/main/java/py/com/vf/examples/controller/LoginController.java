package py.com.vf.examples.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import py.com.vf.examples.bean.User;

@RestController
@RequestMapping("/login")
public class LoginController {

	@PostMapping
	public boolean login(@RequestBody User user) {
		// TODO: Autenticar contra la base con jdbctemplate
		return true;
	}
	
}
